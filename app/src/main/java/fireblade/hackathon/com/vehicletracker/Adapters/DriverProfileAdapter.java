package fireblade.hackathon.com.vehicletracker.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import fireblade.hackathon.com.vehicletracker.Activity.MapsActivity;
import fireblade.hackathon.com.vehicletracker.Models.VehicleDetails;
import fireblade.hackathon.com.vehicletracker.R;
import fireblade.hackathon.com.vehicletracker.Util.AppConfig;
import fireblade.hackathon.com.vehicletracker.Util.Utility;

public class DriverProfileAdapter extends RecyclerView.Adapter<DriverProfileAdapter.MyViewHolder> {

    private List<VehicleDetails> lstDetails;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_srNo, tvDriverName, tvMobile, tvAddress,tvLastLocation;

        public MyViewHolder(View view) {
            super(view);
            tv_srNo = (TextView) view.findViewById(R.id.tv_srNo);
            tvDriverName = (TextView) view.findViewById(R.id.tvDriverName);
            tvMobile = (TextView) view.findViewById(R.id.tvMobile);
            tvAddress = (TextView) view.findViewById(R.id.tvAddress);
            tvLastLocation = (TextView) view.findViewById(R.id.tvLastLocation);
        }
    }


    public DriverProfileAdapter(List<VehicleDetails> list, Context context) {
        this.lstDetails = list;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.drivers_list, parent, false);
        Utility.setFont(context, itemView,
                AppConfig.FONT_APP);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final VehicleDetails details = lstDetails.get(position);
        holder.tv_srNo.setText(position + 1 + ". ");
        holder.tvDriverName.setText(details.getDriverName());
        holder.tvMobile.setText(details.getMobileNo());
        holder.tvAddress.setText(details.getAddress());
        holder.tvLastLocation.setText("Last Location was Goregaon(West),Mumbai");



    }


    @Override
    public int getItemCount() {
        return lstDetails.size();
    }
}