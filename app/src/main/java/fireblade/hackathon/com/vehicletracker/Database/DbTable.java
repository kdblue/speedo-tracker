package fireblade.hackathon.com.vehicletracker.Database;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * @author siddhesh@viraat.info
 * @since 2016
 */
public class DbTable {
    // query to create notification table
    public static String KEY_ROW_ID = "_id";
    public static String KEY_DRIVER_NAME = "d_name";
    public static String KEY_MOBILE_NO = "m_no";
    public static String KEY_ADDRESS = "address";
    public static String KEY_VECHICLE_BRAND = "v_brand";
    public static String KEY_VEHICLE_MODEL = "v_model";
    public static String KEY_LAT = "lat";
    public static String KEY_LON = "lon";
    public static String KEY_LOCATION_DETAILS = "loc_details";
    public static String KEY_DATE_TIME = "date_time";
    public static String KEY_SPEED = "speed";
    public static String KEY_JOURNEY = "journey";
    // table
    public static String createVehicleTable = "create table "
            + DbAdapter.VEHICLE_STATUS_TABLE + "("
            + DbAdapter.KEY_ROW_ID
            + " integer primary key autoincrement,"
            + DbAdapter.KEY_DRIVER_NAME
            + " text ,"
            + DbAdapter.KEY_MOBILE_NO
            + " text ,"
            + DbAdapter.KEY_ADDRESS
            + " text ,"
            + DbAdapter.KEY_VECHICLE_BRAND
            + " text ,"
            + DbAdapter.KEY_VEHICLE_MODEL
            + " text ,"
            + DbAdapter.KEY_LAT
            + " text ,"
            + DbAdapter.KEY_LON
            + " text ,"
            + DbAdapter.KEY_LOCATION_DETAILS
            + " text ,"
            + DbAdapter.KEY_DATE_TIME
            + " text ,"
            + DbAdapter.KEY_SPEED
            + " text ,"
            + DbAdapter.KEY_JOURNEY
            + " text );";









    public static void onCreate(SQLiteDatabase db) {
        db.execSQL(createVehicleTable);

    }

    public static void onUpgrade(SQLiteDatabase db, int Old_Ver, int New_Ver) {
        Log.w(DbTable.class.getName(), "Upgrading DATABASE from version "
                + Old_Ver + " to " + New_Ver
                + ",Which will destroy all old data.");

        db.execSQL("DROP TABLE IF EXISTS " + DbAdapter.VEHICLE_STATUS_TABLE);


        onCreate(db);
    }
}