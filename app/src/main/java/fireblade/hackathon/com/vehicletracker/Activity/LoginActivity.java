package fireblade.hackathon.com.vehicletracker.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.style.TypefaceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import fireblade.hackathon.com.vehicletracker.Database.DbAdapter;
import fireblade.hackathon.com.vehicletracker.Models.VehicleDetails;
import fireblade.hackathon.com.vehicletracker.R;
import fireblade.hackathon.com.vehicletracker.Util.AppConfig;
import fireblade.hackathon.com.vehicletracker.Util.Utility;
//

/**
 * @author siddhesh@viraat.info
 * @since 2016
 */
public class LoginActivity extends AppCompatActivity {

    CoordinatorLayout cl_parent;
    Button btnSignIn;
    public static EditText etEmailOrSms;
    public static View referenceView;
    public static AlertDialog.Builder builder;
    public static Context context;
    public static AlertDialog alertMsg;
    static Typeface robotoRegular;
    static TypefaceSpan robotoRegularSpan;
    TextView tvVersion;
    public static DbAdapter dbAdapter;

    RelativeLayout rl_main;
    //////////////////

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //initialize the static keys

        context = LoginActivity.this;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }

        dbAdapter = new DbAdapter(this);

        if (dbAdapter.fetchVehicleStatsuList().size() == 0) {
            insertDataIntoDb();
        }

        rl_main = (RelativeLayout) findViewById(R.id.rl_main);

        Utility.font = Typeface.createFromAsset(this.getAssets(), AppConfig.FONT_APP);

        Utility.setFont(LoginActivity
                        .this, rl_main,
                AppConfig.FONT_APP);

    }

    public void onForgotPassClick(View view) {
        ShowPopUpWindowForForgotPass();
    }

    public void onLoginClick(View view) {
        Intent in = new Intent(context, MainMenuNavActivity.class);
        startActivity(in);
        finish();
    }

    public void ShowPopUpWindowForForgotPass() {
        try {
            builder = new AlertDialog.Builder(context);

            String msg = "Forgot password?";


            builder.setTitle(msg);
            builder.setView(getlayoutForForgotPass());
            alertMsg = builder.create();
            alertMsg.setCanceledOnTouchOutside(false);
            alertMsg.show();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public View getlayoutForForgotPass() {
        try {
            referenceView = LayoutInflater
                    .from(context)
                    .inflate(R.layout.activity_forgotpass_dialog,
                            (ViewGroup) findViewById(R.id.rl_main_home), false);


            final EditText et_email = (EditText) referenceView.findViewById(R.id.et_email);


            Button submit = (Button) referenceView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertMsg.cancel();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }


        return referenceView;
    }

    private void insertDataIntoDb() {
        ArrayList<VehicleDetails> lstVehicleDetails = new ArrayList<VehicleDetails>();

        VehicleDetails vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("sid");
        vehicleDetails.setMobileNo("7021536166");
        vehicleDetails.setAddress("mumbai");
        vehicleDetails.setVehicleBrand("toyota");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("19.078341922949694");
        vehicleDetails.setLongitude("72.87918090820312");
        vehicleDetails.setLocationDetails("626, Lal Bahadur Shastri Marg, Friends Colony, Kismat Nagar, Kurla, Mumbai, Maharashtra 400070");
        vehicleDetails.setDateTime("22:00");
        vehicleDetails.setJourney("mumbai_to_panvel");
        vehicleDetails.setSpeed("72.5");
        lstVehicleDetails.add(vehicleDetails);


        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("sid");
        vehicleDetails.setMobileNo("7021536166");
        vehicleDetails.setAddress("mumbai");
        vehicleDetails.setVehicleBrand("toyota");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("19.069905620644693");
        vehicleDetails.setLongitude("72.90802001953125");
        vehicleDetails.setLocationDetails("Jeejabai Bhosale Marg, Sector 3, Chedda Nagar, Mumbai, Maharashtra 400089");
        vehicleDetails.setDateTime("23:00");
        vehicleDetails.setJourney("mumbai_to_panvel");
        vehicleDetails.setSpeed("60.5");
        lstVehicleDetails.add(vehicleDetails);


        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("sid");
        vehicleDetails.setMobileNo("7021536166");
        vehicleDetails.setAddress("mumbai");
        vehicleDetails.setVehicleBrand("toyota");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("19.05173366503917");
        vehicleDetails.setLongitude("72.93548583984375");
        vehicleDetails.setLocationDetails("5, Jeejabai Bhosale Marg, Chikuwadi, Mankhurd, Mumbai, Maharashtra 400043");
        vehicleDetails.setDateTime("24:00");
        vehicleDetails.setJourney("mumbai_to_panvel");
        vehicleDetails.setSpeed("90");
        lstVehicleDetails.add(vehicleDetails);


        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("sid");
        vehicleDetails.setMobileNo("7021536166");
        vehicleDetails.setAddress("mumbai");
        vehicleDetails.setVehicleBrand("toyota");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("19.062766875581328");
        vehicleDetails.setLongitude("72.9766845703125");
        vehicleDetails.setLocationDetails("Sion - Panvel Expy, Sector-31, Vashi, Navi Mumbai, Maharashtra 400703");
        vehicleDetails.setDateTime("1:00");
        vehicleDetails.setJourney("mumbai_to_panvel");
        vehicleDetails.setSpeed("60.5");
        lstVehicleDetails.add(vehicleDetails);


        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("sid");
        vehicleDetails.setMobileNo("7021536166");
        vehicleDetails.setAddress("mumbai");
        vehicleDetails.setVehicleBrand("toyota");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("19.067309748918017");
        vehicleDetails.setLongitude("73.01856994628906");
        vehicleDetails.setLocationDetails("34, Sion - Panvel Expy, Sector 24, Turbhe, Navi Mumbai, Maharashtra 400703");
        vehicleDetails.setDateTime("2:00");
        vehicleDetails.setJourney("mumbai_to_panvel");
        vehicleDetails.setSpeed("100");
        lstVehicleDetails.add(vehicleDetails);


        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("sid");
        vehicleDetails.setMobileNo("7021536166");
        vehicleDetails.setAddress("mumbai");
        vehicleDetails.setVehicleBrand("toyota");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("19.04913750885002");
        vehicleDetails.setLongitude("73.02337646484375");
        vehicleDetails.setLocationDetails("Thane-Belapur Rd, MIDC Industrial Area, Shiravane, Nerul, Navi Mumbai, Maharashtra 400706");
        vehicleDetails.setDateTime("3:00");
        vehicleDetails.setJourney("mumbai_to_panvel");
        vehicleDetails.setSpeed("75");
        lstVehicleDetails.add(vehicleDetails);


        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("sid");
        vehicleDetails.setMobileNo("7021536166");
        vehicleDetails.setAddress("mumbai");
        vehicleDetails.setVehicleBrand("toyota");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("19.027068541735314");
        vehicleDetails.setLongitude("73.03642272949219");
        vehicleDetails.setLocationDetails("Sion - Panvel Expy, Police Line, Sector 9, CBD Belapur, Navi Mumbai, Maharashtra 400614");
        vehicleDetails.setDateTime("4:00");
        vehicleDetails.setJourney("mumbai_to_panvel");
        vehicleDetails.setSpeed("78.5");
        lstVehicleDetails.add(vehicleDetails);


        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("sid");
        vehicleDetails.setMobileNo("7021536166");
        vehicleDetails.setAddress("mumbai");
        vehicleDetails.setVehicleBrand("toyota");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("19.03680521233228");
        vehicleDetails.setLongitude("73.07212829589844");
        vehicleDetails.setLocationDetails("Sion - Panvel Expy, Sector 11, Kharghar, Navi Mumbai, Maharashtra 410210");
        vehicleDetails.setDateTime("5:00");
        vehicleDetails.setJourney("mumbai_to_panvel");
        vehicleDetails.setSpeed("69.5");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("sid");
        vehicleDetails.setMobileNo("7021536166");
        vehicleDetails.setAddress("mumbai");
        vehicleDetails.setVehicleBrand("toyota");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("19.025121139131258");
        vehicleDetails.setLongitude("73.09616088867188");
        vehicleDetails.setLocationDetails("16, Sion - Panvel Expy, Jadhav Wadi, Sector - 1, Kalamboli, Panvel, Navi Mumbai, Maharashtra 410218");
        vehicleDetails.setDateTime("6:00");
        vehicleDetails.setJourney("mumbai_to_panvel");
        vehicleDetails.setSpeed("70.5");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("sid");
        vehicleDetails.setMobileNo("7021536166");
        vehicleDetails.setAddress("mumbai");
        vehicleDetails.setVehicleBrand("toyota");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("18.990063991345178");
        vehicleDetails.setLongitude("73.11470031738281");
        vehicleDetails.setLocationDetails("Zunka Bhakar Rd, Old Panvel, Panvel, Navi Mumbai, Maharashtra 410206");
        vehicleDetails.setDateTime("7:00");
        vehicleDetails.setJourney("mumbai_to_panvel");
        vehicleDetails.setSpeed("78.5");
        lstVehicleDetails.add(vehicleDetails);


        // <!--========== starting of pune to karjat=========-->

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("kuldeep");
        vehicleDetails.setMobileNo("8097326686");
        vehicleDetails.setAddress("Irla");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("18.521283325496277");
        vehicleDetails.setLongitude("73.861083984375");
        vehicleDetails.setLocationDetails("340-375, Maharana Pratap Rd, Somwar Peth, Pune, Maharashtra 411011");
        vehicleDetails.setDateTime("7:00");
        vehicleDetails.setJourney("pune_to_karjat");
        vehicleDetails.setSpeed("88");
        lstVehicleDetails.add(vehicleDetails);


        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("kuldeep");
        vehicleDetails.setMobileNo("8097326686");
        vehicleDetails.setAddress("Irla");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("18.495238095433262");
        vehicleDetails.setLongitude("73.9984130859375");
        vehicleDetails.setLocationDetails("Solapur - Pune Hwy, Maharashtra 412307");
        vehicleDetails.setDateTime("7:30");
        vehicleDetails.setJourney("pune_to_karjat");
        vehicleDetails.setSpeed("78");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("kuldeep");
        vehicleDetails.setMobileNo("8097326686");
        vehicleDetails.setAddress("Irla");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("18.48742375381096");
        vehicleDetails.setLongitude("74.1302490234375");
        vehicleDetails.setLocationDetails("Solapur - Pune Hwy, Yavat, Uruli Kanchan, Maharashtra 412202");
        vehicleDetails.setDateTime("8:00");
        vehicleDetails.setJourney("pune_to_karjat");
        vehicleDetails.setSpeed("75");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("kuldeep");
        vehicleDetails.setMobileNo("8097326686");
        vehicleDetails.setAddress("Irla");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("18.47960905583197");
        vehicleDetails.setLongitude("74.2401123046875");
        vehicleDetails.setLocationDetails("Solapur - Pune Hwy, Maharashtra 412214");
        vehicleDetails.setDateTime("8:30");
        vehicleDetails.setJourney("pune_to_karjat");
        vehicleDetails.setSpeed("70");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("kuldeep");
        vehicleDetails.setMobileNo("8097326686");
        vehicleDetails.setAddress("Irla");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("18.45616282504624");
        vehicleDetails.setLongitude("74.37469482421875");
        vehicleDetails.setLocationDetails("Solapur - Pune Hwy, Kedagaon, Maharashtra 412203");
        vehicleDetails.setDateTime("9:00");
        vehicleDetails.setJourney("pune_to_karjat");
        vehicleDetails.setSpeed("75");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("kuldeep");
        vehicleDetails.setMobileNo("8097326686");
        vehicleDetails.setAddress("Irla");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("18.401442504848262");
        vehicleDetails.setLongitude("74.49554443359375");
        vehicleDetails.setLocationDetails("Unnamed Road, Maharashtra 412219");
        vehicleDetails.setDateTime("9:30");
        vehicleDetails.setJourney("pune_to_karjat");
        vehicleDetails.setSpeed("70");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("kuldeep");
        vehicleDetails.setMobileNo("8097326686");
        vehicleDetails.setAddress("Irla");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("18.445741249840815");
        vehicleDetails.setLongitude("74.6246337890625");
        vehicleDetails.setLocationDetails("MH SH 67, Daund, Maharashtra 413801");
        vehicleDetails.setDateTime("10:00");
        vehicleDetails.setJourney("pune_to_karjat");
        vehicleDetails.setSpeed("76");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("kuldeep");
        vehicleDetails.setMobileNo("8097326686");
        vehicleDetails.setAddress("Irla");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("18.453557490539584");
        vehicleDetails.setLongitude("74.7509765625");
        vehicleDetails.setLocationDetails("MH SH 67, Bhambore, Maharashtra 414403");
        vehicleDetails.setDateTime("10:30");
        vehicleDetails.setJourney("pune_to_karjat");
        vehicleDetails.setSpeed("77");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("kuldeep");
        vehicleDetails.setMobileNo("8097326686");
        vehicleDetails.setAddress("Irla");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("18.445741249840815");
        vehicleDetails.setLongitude("74.86358642578125");
        vehicleDetails.setLocationDetails("MH SH 67, Yeswadi, Maharashtra 414403");
        vehicleDetails.setDateTime("11:00");
        vehicleDetails.setJourney("pune_to_karjat");
        vehicleDetails.setSpeed("75");
        lstVehicleDetails.add(vehicleDetails);


        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("kuldeep");
        vehicleDetails.setMobileNo("8097326686");
        vehicleDetails.setAddress("Irla");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("18.477004077337586");
        vehicleDetails.setLongitude("74.95697021484375");
        vehicleDetails.setLocationDetails("Kangudwadi-Deshmukhwadi Road, Kangudwadi, Maharashtra 414403");
        vehicleDetails.setDateTime("12:00");
        vehicleDetails.setJourney("pune_to_karjat");
        vehicleDetails.setSpeed("90");
        lstVehicleDetails.add(vehicleDetails);


        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("kuldeep");
        vehicleDetails.setMobileNo("8097326686");
        vehicleDetails.setAddress("Irla");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("18.562947442888312");
        vehicleDetails.setLongitude("75.00640869140625");
        vehicleDetails.setLocationDetails("Mirajgaon Rd, Karjat, Maharashtra 414402");
        vehicleDetails.setDateTime("12:30");
        vehicleDetails.setJourney("pune_to_karjat");
        vehicleDetails.setSpeed("70");
        lstVehicleDetails.add(vehicleDetails);


        //<!-----------starting of hydrabaad to nagpur------>

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("Saurabh");
        vehicleDetails.setMobileNo("8097329000");
        vehicleDetails.setAddress("Bhayendar");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("17.392579271057766");
        vehicleDetails.setLongitude("78.486328125");
        vehicleDetails.setLocationDetails("Padmashali Bhavan,Lane Opp to Blood Bank,Rajmohalla, Ramkoti, King Koti, Narayanguda, Hyderabad, Telangana 500029");
        vehicleDetails.setDateTime("4:00");
        vehicleDetails.setJourney("hydrabaad_to_nagpur");
        vehicleDetails.setSpeed("50");
        lstVehicleDetails.add(vehicleDetails);


        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("Saurabh");
        vehicleDetails.setMobileNo("8097329000");
        vehicleDetails.setAddress("Bhayendar");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("18.004855857908034");
        vehicleDetails.setLongitude("78.4478759765625");
        vehicleDetails.setLocationDetails("Srinagar - Kanyakumari Hwy, Bheemraopalle, Telangana 502255");
        vehicleDetails.setDateTime("4:30");
        vehicleDetails.setJourney("hydrabaad_to_nagpur");
        vehicleDetails.setSpeed("55");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("Saurabh");
        vehicleDetails.setMobileNo("8097329000");
        vehicleDetails.setAddress("Bhayendar");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("18.401442504848262");
        vehicleDetails.setLongitude("78.277587890625");
        vehicleDetails.setLocationDetails("Srinagar - Kanyakumari Hwy, Telangana 503145");
        vehicleDetails.setDateTime("5:00");
        vehicleDetails.setJourney("hydrabaad_to_nagpur");
        vehicleDetails.setSpeed("65");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("Saurabh");
        vehicleDetails.setMobileNo("8097329000");
        vehicleDetails.setAddress("Bhayendar");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("18.953051214527225");
        vehicleDetails.setLongitude("78.3709716796875");
        vehicleDetails.setLocationDetails("Srinagar - Kanyakumari Hwy, Telangana 503219");
        vehicleDetails.setDateTime("5:30");
        vehicleDetails.setJourney("hydrabaad_to_nagpur");
        vehicleDetails.setSpeed("70");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("Saurabh");
        vehicleDetails.setMobileNo("8097329000");
        vehicleDetails.setAddress("Bhayendar");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("19.35779359620928");
        vehicleDetails.setLongitude("78.4259033203125");
        vehicleDetails.setLocationDetails("Srinagar - Kanyakumari Hwy, Kupti, Telangana 504307");
        vehicleDetails.setDateTime("6:00");
        vehicleDetails.setJourney("hydrabaad_to_nagpur");
        vehicleDetails.setSpeed("90");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("Saurabh");
        vehicleDetails.setMobileNo("8097329000");
        vehicleDetails.setAddress("Bhayendar");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("19.751194318940865");
        vehicleDetails.setLongitude("78.5577392578125");
        vehicleDetails.setLocationDetails("Srinagar - Kanyakumari Hwy, Poosai, Telangana 504309");
        vehicleDetails.setDateTime("6:30");
        vehicleDetails.setJourney("hydrabaad_to_nagpur");
        vehicleDetails.setSpeed("70");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("Saurabh");
        vehicleDetails.setMobileNo("8097329000");
        vehicleDetails.setAddress("Bhayendar");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("20.107523268824004");
        vehicleDetails.setLongitude("78.5797119140625");
        vehicleDetails.setLocationDetails("Srinagar - Kanyakumari Hwy, Dharna, Maharashtra 445302");
        vehicleDetails.setDateTime("7:00");
        vehicleDetails.setJourney("hydrabaad_to_nagpur");
        vehicleDetails.setSpeed("69");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("Saurabh");
        vehicleDetails.setMobileNo("8097329000");
        vehicleDetails.setAddress("Bhayendar");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("20.33947636615292");
        vehicleDetails.setLongitude("78.717041015625");
        vehicleDetails.setLocationDetails("Srinagar - Kanyakumari Hwy, Maharashtra 442307");
        vehicleDetails.setDateTime("7:30");
        vehicleDetails.setJourney("hydrabaad_to_nagpur");
        vehicleDetails.setSpeed("55");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("Saurabh");
        vehicleDetails.setMobileNo("8097329000");
        vehicleDetails.setAddress("Bhayendar");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("20.648205934292623");
        vehicleDetails.setLongitude("78.9312744140625");
        vehicleDetails.setLocationDetails("SH258, Shedgaon Pati, Maharashtra 442305");
        vehicleDetails.setDateTime("8:00");
        vehicleDetails.setJourney("hydrabaad_to_nagpur");
        vehicleDetails.setSpeed("59");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("Saurabh");
        vehicleDetails.setMobileNo("8097329000");
        vehicleDetails.setAddress("Bhayendar");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("20.910134481692683");
        vehicleDetails.setLongitude("78.99169921875");
        vehicleDetails.setLocationDetails("Mancherial - Chandrapur - Nagpur Rd, Patan Bori, Maharashtra 441108");
        vehicleDetails.setDateTime("8:30");
        vehicleDetails.setJourney("hydrabaad_to_nagpur");
        vehicleDetails.setSpeed("60");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("Saurabh");
        vehicleDetails.setMobileNo("8097329000");
        vehicleDetails.setAddress("Bhayendar");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("21.140868798573802");
        vehicleDetails.setLongitude("79.090576171875");
        vehicleDetails.setLocationDetails("1, Ghat Rd, Gaitry Sadan, Ganeshpeth Colony, Nagpur, Maharashtra 440018");
        vehicleDetails.setDateTime("9:00");
        vehicleDetails.setJourney("hydrabaad_to_nagpur");
        vehicleDetails.setSpeed("70");
        lstVehicleDetails.add(vehicleDetails);


        // <!----- starting bhopal to jabalpur----->


        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("mihir");
        vehicleDetails.setMobileNo("8787029000");
        vehicleDetails.setAddress("vasai");
        vehicleDetails.setVehicleBrand("maruti");
        vehicleDetails.setVehicleModel("i10");
        vehicleDetails.setLatitude("23.25648743787913");
        vehicleDetails.setLongitude("77.4151611328125");
        vehicleDetails.setLocationDetails("62, Aishbagh Rd, Jahangirabad, Bhopal, Madhya Pradesh 462008");
        vehicleDetails.setDateTime("2:00");
        vehicleDetails.setJourney("bhopal to jabalpur");
        vehicleDetails.setSpeed("50");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("mihir");
        vehicleDetails.setMobileNo("8787029000");
        vehicleDetails.setAddress("Bhayendar");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("23.00390802663066");
        vehicleDetails.setLongitude("77.58544921875");
        vehicleDetails.setLocationDetails("Hoshangabad Rd, Obaidullaganj, Madhya Pradesh 464993");
        vehicleDetails.setDateTime("2:30");
        vehicleDetails.setJourney("bhopal to jabalpur");
        vehicleDetails.setSpeed("55");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("mihir");
        vehicleDetails.setMobileNo("8787029000");
        vehicleDetails.setAddress("Bhayendar");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("23.104996849988808");
        vehicleDetails.setLongitude("77.8985595703125");
        vehicleDetails.setLocationDetails("Gouharganj Rd, Khamriya Nimawar, Madhya Pradesh 464986");
        vehicleDetails.setDateTime("3:00");
        vehicleDetails.setJourney("bhopal to jabalpur");
        vehicleDetails.setSpeed("90");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("mihir");
        vehicleDetails.setMobileNo("8787029000");
        vehicleDetails.setAddress("Bhayendar");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("23.01402032323799");
        vehicleDetails.setLongitude("78.101806640625");
        vehicleDetails.setLocationDetails("Unnamed Road, Badodiya Khurd, Madhya Pradesh 464665");
        vehicleDetails.setDateTime("3:30");
        vehicleDetails.setJourney("bhopal to jabalpur");
        vehicleDetails.setSpeed("70");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("mihir");
        vehicleDetails.setMobileNo("8787029000");
        vehicleDetails.setAddress("Bhayendar");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("23.0999442125314");
        vehicleDetails.setLongitude("78.343505859375");
        vehicleDetails.setLocationDetails("Gouharganj Rd, Madhya Pradesh 464672");
        vehicleDetails.setDateTime("4:00");
        vehicleDetails.setJourney("bhopal to jabalpur");
        vehicleDetails.setSpeed("80");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("mihir");
        vehicleDetails.setMobileNo("8787029000");
        vehicleDetails.setAddress("Bhayendar");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("23.104996849988808");
        vehicleDetails.setLongitude("78.651123046875");
        vehicleDetails.setLocationDetails("Gouharganj Rd, Pipaliya Kalan, Madhya Pradesh 464776");
        vehicleDetails.setDateTime("4:30");
        vehicleDetails.setJourney("bhopal to jabalpur");
        vehicleDetails.setSpeed("70");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("mihir");
        vehicleDetails.setMobileNo("8787029000");
        vehicleDetails.setAddress("Bhayendar");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("23.130257185291036");
        vehicleDetails.setLongitude("78.99169921875");
        vehicleDetails.setLocationDetails("Unnamed Road, Bamhori, Madhya Pradesh 487330");
        vehicleDetails.setDateTime("5:00");
        vehicleDetails.setJourney("bhopal to jabalpur");
        vehicleDetails.setSpeed("55");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("mihir");
        vehicleDetails.setMobileNo("8787029000");
        vehicleDetails.setAddress("Bhayendar");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("23.12520549860231");
        vehicleDetails.setLongitude("79.310302734375");
        vehicleDetails.setLocationDetails("Unnamed Road, Amoda, Madhya Pradesh 487330");
        vehicleDetails.setDateTime("5:30");
        vehicleDetails.setJourney("bhopal to jabalpur");
        vehicleDetails.setSpeed("60");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("mihir");
        vehicleDetails.setMobileNo("8787029000");
        vehicleDetails.setAddress("Bhayendar");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("23.160563309048314");
        vehicleDetails.setLongitude("79.6453857421875");
        vehicleDetails.setLocationDetails("Unnamed Road, Suhajani, Madhya Pradesh 483119");
        vehicleDetails.setDateTime("6:00");
        vehicleDetails.setJourney("bhopal to jabalpur");
        vehicleDetails.setSpeed("57");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("mihir");
        vehicleDetails.setMobileNo("8787029000");
        vehicleDetails.setAddress("Bhayendar");
        vehicleDetails.setVehicleBrand("indica");
        vehicleDetails.setVehicleModel("fortuner");
        vehicleDetails.setLatitude("23.19086257687362");
        vehicleDetails.setLongitude("79.991455078125");
        vehicleDetails.setLocationDetails("MP SH 22, Gokulpur, Jabalpur, Madhya Pradesh 482011");
        vehicleDetails.setDateTime("6:30");
        vehicleDetails.setJourney("bhopal to jabalpur");
        vehicleDetails.setSpeed("70");
        lstVehicleDetails.add(vehicleDetails);


        //<!-----starting of jhansi to kanpur ---->


        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("sameer");
        vehicleDetails.setMobileNo("8783339000");
        vehicleDetails.setAddress("Virar");
        vehicleDetails.setVehicleBrand("mahindra");
        vehicleDetails.setVehicleModel("suv");
        vehicleDetails.setLatitude("25.44823489808649");
        vehicleDetails.setLongitude("78.5687255859375");
        vehicleDetails.setLocationDetails("Shivpuri - Jhansi Rd, Civil Lines, Cantt, Jhansi, Uttar Pradesh 284001");
        vehicleDetails.setDateTime("13:30");
        vehicleDetails.setJourney("jhansi to kanpur");
        vehicleDetails.setSpeed("40");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("sameer");
        vehicleDetails.setMobileNo("8783339000");
        vehicleDetails.setAddress("Virar");
        vehicleDetails.setVehicleBrand("mahindra");
        vehicleDetails.setVehicleModel("suv");
        vehicleDetails.setLatitude("25.63657407787705");
        vehicleDetails.setLongitude("78.8763427734375");
        vehicleDetails.setLocationDetails("Jhansi-Kanpur Hwy, Karguwan, Uttar Pradesh 284303");
        vehicleDetails.setDateTime("13:40");
        vehicleDetails.setJourney("jhansi to kanpur");
        vehicleDetails.setSpeed("56");
        lstVehicleDetails.add(vehicleDetails);


        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("sameer");
        vehicleDetails.setMobileNo("8783339000");
        vehicleDetails.setAddress("Virar");
        vehicleDetails.setVehicleBrand("mahindra");
        vehicleDetails.setVehicleModel("suv");
        vehicleDetails.setLatitude("25.80483668216131");
        vehicleDetails.setLongitude("79.024658203125");
        vehicleDetails.setLocationDetails("Jhansi-Kanpur Hwy, Khilli Jhansi, Uttar Pradesh 284306");
        vehicleDetails.setDateTime("13:50");
        vehicleDetails.setJourney("jhansi to kanpur");
        vehicleDetails.setSpeed("60");
        lstVehicleDetails.add(vehicleDetails);


        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("sameer");
        vehicleDetails.setMobileNo("8783339000");
        vehicleDetails.setAddress("Virar");
        vehicleDetails.setVehicleBrand("mahindra");
        vehicleDetails.setVehicleModel("suv");
        vehicleDetails.setLatitude("25.923466700919274");
        vehicleDetails.setLongitude("79.3212890625");
        vehicleDetails.setLocationDetails("Jhansi-Kanpur Hwy, Nunsai, Uttar Pradesh 285001");
        vehicleDetails.setDateTime("14:00");
        vehicleDetails.setJourney("jhansi to kanpur");
        vehicleDetails.setSpeed("70");
        lstVehicleDetails.add(vehicleDetails);


        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("sameer");
        vehicleDetails.setMobileNo("8783339000");
        vehicleDetails.setAddress("Virar");
        vehicleDetails.setVehicleBrand("mahindra");
        vehicleDetails.setVehicleModel("suv");
        vehicleDetails.setLatitude("26.04197744797015");
        vehicleDetails.setLongitude("79.5904541015625");
        vehicleDetails.setLocationDetails("Jhansi-Kanpur Hwy, Aata, Uttar Pradesh 285202");
        vehicleDetails.setDateTime("14:10");
        vehicleDetails.setJourney("jhansi to kanpur");
        vehicleDetails.setSpeed("75");
        lstVehicleDetails.add(vehicleDetails);


        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("sameer");
        vehicleDetails.setMobileNo("8783339000");
        vehicleDetails.setAddress("Virar");
        vehicleDetails.setVehicleBrand("mahindra");
        vehicleDetails.setVehicleModel("suv");
        vehicleDetails.setLatitude("26.1800887243873");
        vehicleDetails.setLongitude("79.793701171875");
        vehicleDetails.setLocationDetails("Jhansi-Kanpur Hwy, Jalpura, Uttar Pradesh 209112");
        vehicleDetails.setDateTime("14:20");
        vehicleDetails.setJourney("jhansi to kanpur");
        vehicleDetails.setSpeed("90");
        lstVehicleDetails.add(vehicleDetails);


        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("sameer");
        vehicleDetails.setMobileNo("8783339000");
        vehicleDetails.setAddress("Virar");
        vehicleDetails.setVehicleBrand("mahindra");
        vehicleDetails.setVehicleModel("suv");
        vehicleDetails.setLatitude("26.37218544169559");
        vehicleDetails.setLongitude("79.991455078125");
        vehicleDetails.setLocationDetails("Jhansi-Kanpur Hwy, Industrial Area Phase 1, Banar Ali, Uttar Pradesh 209311");
        vehicleDetails.setDateTime("14:30");
        vehicleDetails.setJourney("jhansi to kanpur");
        vehicleDetails.setSpeed("70");
        lstVehicleDetails.add(vehicleDetails);

        vehicleDetails = new VehicleDetails();
        vehicleDetails.setDriverName("sameer");
        vehicleDetails.setMobileNo("8783339000");
        vehicleDetails.setAddress("Virar");
        vehicleDetails.setVehicleBrand("mahindra");
        vehicleDetails.setVehicleModel("suv");
        vehicleDetails.setLatitude("26.44598399885638");
        vehicleDetails.setLongitude("80.3375244140625");
        vehicleDetails.setLocationDetails("133/118, Near Plus Building, Transport Nagar, Cooperganj, Kanpur, Uttar Pradesh 208023");
        vehicleDetails.setDateTime("14:40");
        vehicleDetails.setJourney("jhansi to kanpur");
        vehicleDetails.setSpeed("74");
        lstVehicleDetails.add(vehicleDetails);


        dbAdapter.insertVehicleStatusDetails(lstVehicleDetails);
    }

}
