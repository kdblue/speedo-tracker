package fireblade.hackathon.com.vehicletracker.Activity;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.logging.Handler;

import fireblade.hackathon.com.vehicletracker.Models.VehicleDetails;
import fireblade.hackathon.com.vehicletracker.R;
import fireblade.hackathon.com.vehicletracker.Util.Utility;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    //    public static TabLayout tabLayout;
    public static Toolbar toolbar;
    public static AppBarLayout appBarLayout;
    TextView tv_Driver, tvMobileNo, tvCar, tvJourney;
    public static VehicleDetails vehicleDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);

        appBarLayout = (AppBarLayout) findViewById(R.id.appBarLayout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Journey Details");
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        tv_Driver = (TextView) findViewById(R.id.tv_Driver);
        tvMobileNo = (TextView) findViewById(R.id.tvMobileNo);
        tvCar = (TextView) findViewById(R.id.tvCar);
        tvJourney = (TextView) findViewById(R.id.tvJourney);


        if (!Utility.isInternetConnectionAvailable(this)) {
            Toast.makeText(this, "Please Turn On Your Internet", Toast.LENGTH_SHORT).show();
        }
        bindDriversDetails();

    }

    private void bindDriversDetails() {
        tv_Driver.setText("Drivers Name: " + vehicleDetails.getDriverName());
        tvCar.setText("Car: " + vehicleDetails.getVehicleBrand() + " " + vehicleDetails.getVehicleModel());
        tvMobileNo.setText("Mobile No: " + vehicleDetails.getMobileNo());
        tvJourney.setText("Journey: " + vehicleDetails.getJourney());
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add a marker in Sydney and move the camera


        String journey = vehicleDetails.getJourney();

        ArrayList<VehicleDetails> lstDetails = LoginActivity.dbAdapter.fetchVehicleStatusByJourneyList(journey);
        LatLng latlong = null;
        for (int i = 0; i < lstDetails.size(); i++) {
            LatLng latlongMarker = new LatLng(Double.parseDouble(lstDetails.get(i).getLatitude()), Double.parseDouble(lstDetails.get(i).getLongitude()));
            mMap.addMarker(new MarkerOptions().position(latlongMarker).title(lstDetails.get(i).getLocationDetails())).setSnippet(lstDetails.get(i).getSpeed() + " km/hr");

            if (i == 5) {
                latlong = latlongMarker;
            }

            Float speed = Float.parseFloat(lstDetails.get(i).getSpeed());
            if (speed.intValue() > 80) {
                showNotification(MapsActivity.this, "Drivers Status", vehicleDetails.getDriverName() + " is driving above 80km/hr.", vehicleDetails.getMobileNo());
            }
        }


        if (latlong != null) {

            mMap.moveCamera(CameraUpdateFactory.newLatLng(latlong));
            CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(latlong, 7);
            mMap.animateCamera(yourLocation);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT_WATCH)
    public static void showNotification(Context context, String title, String body, String mobileNo) {
        // build notification
// the addAction re-use the same intent to keep the example short


        Intent intentMain = new Intent(Intent.ACTION_CALL);
        intentMain.setData(Uri.parse("tel: " + mobileNo));
        PendingIntent pIntentMain = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), intentMain, 0);


        Intent intentHelp = new Intent(Intent.ACTION_CALL);
        intentHelp.setData(Uri.parse("tel: " + mobileNo));
        PendingIntent pIntentHelp = PendingIntent.getService(context, (int) System.currentTimeMillis(), intentHelp, 0);

// build notification
// the addAction re-use the same intent to keep the example short
        Notification n = new Notification.Builder(context)
                .setPriority(Notification.PRIORITY_MAX)
                .setContentTitle(title)
                .setContentText(body)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(null)
                .setAutoCancel(true)
                .setVibrate(new long[]{1000, 1000, 1000, 1000})
                .setLights(Color.RED, 3000, 3000)
                .setContentIntent(pIntentMain)
                .addAction(R.mipmap.ic_call_black_24dp, "Call him", pIntentHelp).build();


        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);

        notificationManager.notify(1, n);


    }
    @Override
    public void onBackPressed() {
        // app icon in action bar clicked; go home
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(1);
        super.onBackPressed();
    }
}
