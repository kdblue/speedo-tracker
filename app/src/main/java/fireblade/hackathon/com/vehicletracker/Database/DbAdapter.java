package fireblade.hackathon.com.vehicletracker.Database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import fireblade.hackathon.com.vehicletracker.Models.VehicleDetails;

/**
 * @author siddhesh@viraat.info
 * @since 2016
 */
public class DbAdapter {


    // table names
    public static String VEHICLE_STATUS_TABLE = "notification_tbl";


    // Table column names
    public static String KEY_ROW_ID = "_id";
    public static String KEY_DRIVER_NAME = "d_name";
    public static String KEY_MOBILE_NO = "m_no";
    public static String KEY_ADDRESS = "address";
    public static String KEY_VECHICLE_BRAND = "v_brand";
    public static String KEY_VEHICLE_MODEL = "v_model";
    public static String KEY_LAT = "lat";
    public static String KEY_LON = "lon";
    public static String KEY_LOCATION_DETAILS = "loc_details";
    public static String KEY_DATE_TIME = "date_time";
    public static String KEY_SPEED = "speed";
    public static String KEY_JOURNEY = "journey";


    // private static SQLiteDatabase db;
    private static DbHelper dbHelper;

    public DbAdapter(Context context) {
        dbHelper = new DbHelper(context);
    }

    // ==========================================================================================

    // method to put data in content value for vehicles
    public static ContentValues CreateVehicleDetailsValues(VehicleDetails details) {
        ContentValues value = new ContentValues();
        value.put(KEY_DRIVER_NAME, details.getDriverName().trim());
        value.put(KEY_MOBILE_NO, details.getMobileNo().trim());
        value.put(KEY_ADDRESS, details.getAddress().trim());
        value.put(KEY_VECHICLE_BRAND, details.getVehicleBrand().trim());
        value.put(KEY_VEHICLE_MODEL, details.getVehicleModel().trim());
        value.put(KEY_LAT, details.getLatitude().trim());
        value.put(KEY_LON, details.getLongitude().trim());
        value.put(KEY_LOCATION_DETAILS, details.getLocationDetails().trim());
        value.put(KEY_DATE_TIME, details.getDateTime().trim());
        value.put(KEY_SPEED, details.getSpeed().trim());
        value.put(KEY_JOURNEY, details.getJourney().trim());

        return value;
    }


    // ==========================================================================================
    // method to insert into table
    public synchronized long insertVehicleStatusDetails(ArrayList<VehicleDetails> lstDetails) {
        long rowId = 0;
        SQLiteDatabase db = null;

        // Trying to open writable database
        try {
            db = dbHelper.getWritableDatabase();
            if (db != null) {

                db.beginTransaction();
                for (int i = 0; i < lstDetails.size(); i++) {
                    ContentValues Value = CreateVehicleDetailsValues(lstDetails.get(i));
                    rowId = db.insert(VEHICLE_STATUS_TABLE, null, Value);
                    Log.d("inserting vehicle: ", String.valueOf(i));
                }


                db.setTransactionSuccessful();
                db.endTransaction();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Trying to close writable database
        try {
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rowId;
    }


    //===========================================================================

    // fetch data from table
    public synchronized ArrayList<VehicleDetails> fetchVehicleStatsuList() {

        ArrayList<VehicleDetails> lstDetails = new ArrayList<VehicleDetails>();

        SQLiteDatabase db = null;
        Cursor fetchCur = null;
        // Trying to open readable database
        try {

            db = dbHelper.getReadableDatabase();
            if (db != null) {
                fetchCur = db.query(true, VEHICLE_STATUS_TABLE, null,
                        null
                        , null
                        , null, null, KEY_ROW_ID + " DESC", null);

                if (fetchCur != null) {
                    if (fetchCur.moveToFirst()) {
                        do {


                            VehicleDetails details = new VehicleDetails();

                            details.setRowId(fetchCur.getInt(fetchCur
                                    .getColumnIndexOrThrow(KEY_ROW_ID)));


                            details.setDriverName(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_DRIVER_NAME)));
                            details.setMobileNo(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_MOBILE_NO)));
                            details.setAddress(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_ADDRESS)));
                            details.setVehicleBrand(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_VECHICLE_BRAND)));
                            details.setVehicleModel(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_VEHICLE_MODEL)));
                            details.setLatitude(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_LAT)));
                            details.setLongitude(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_LON)));
                            details.setLocationDetails(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_LOCATION_DETAILS)));
                            details.setDateTime(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_DATE_TIME)));
                            details.setSpeed(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_SPEED)));
                            details.setJourney(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_JOURNEY)));


                            lstDetails.add(details);

                        } while (fetchCur.moveToNext());
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Trying to close readable database
        try {

            fetchCur.close();
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lstDetails;
    }


    // fetch data from table
    public synchronized ArrayList<VehicleDetails> fetchVehicleStatusByJourneyList(String journey) {

        ArrayList<VehicleDetails> lstDetails = new ArrayList<VehicleDetails>();

        SQLiteDatabase db = null;
        Cursor fetchCur = null;
        // Trying to open readable database
        try {

            db = dbHelper.getReadableDatabase();
            if (db != null) {
                fetchCur = db.query(true, VEHICLE_STATUS_TABLE, null,
                        KEY_JOURNEY + "='" + journey + "'"
                        , null
                        , null, null, KEY_ROW_ID + " DESC", null);

                if (fetchCur != null) {
                    if (fetchCur.moveToFirst()) {
                        do {


                            VehicleDetails details = new VehicleDetails();

                            details.setRowId(fetchCur.getInt(fetchCur
                                    .getColumnIndexOrThrow(KEY_ROW_ID)));


                            details.setDriverName(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_DRIVER_NAME)));
                            details.setMobileNo(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_MOBILE_NO)));
                            details.setAddress(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_ADDRESS)));
                            details.setVehicleBrand(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_VECHICLE_BRAND)));
                            details.setVehicleModel(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_VEHICLE_MODEL)));
                            details.setLatitude(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_LAT)));
                            details.setLongitude(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_LON)));
                            details.setLocationDetails(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_LOCATION_DETAILS)));
                            details.setDateTime(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_DATE_TIME)));
                            details.setSpeed(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_SPEED)));
                            details.setJourney(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_JOURNEY)));


                            lstDetails.add(details);

                        } while (fetchCur.moveToNext());
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Trying to close readable database
        try {

            fetchCur.close();
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lstDetails;
    }





    // fetch data from table
    public synchronized ArrayList<VehicleDetails> fetchDistinctDriverList() {

        ArrayList<VehicleDetails> lstDetails = new ArrayList<VehicleDetails>();

        SQLiteDatabase db = null;
        Cursor fetchCur = null;
        // Trying to open readable database
        try {

            db = dbHelper.getReadableDatabase();
            if (db != null) {
                fetchCur = db.query(true, VEHICLE_STATUS_TABLE, null,
                     null
                        , null
                        , KEY_DRIVER_NAME, null, KEY_ROW_ID + " DESC", null);

                if (fetchCur != null) {
                    if (fetchCur.moveToFirst()) {
                        do {


                            VehicleDetails details = new VehicleDetails();

                            details.setRowId(fetchCur.getInt(fetchCur
                                    .getColumnIndexOrThrow(KEY_ROW_ID)));


                            details.setDriverName(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_DRIVER_NAME)));
                            details.setMobileNo(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_MOBILE_NO)));
                            details.setAddress(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_ADDRESS)));
                            details.setVehicleBrand(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_VECHICLE_BRAND)));
                            details.setVehicleModel(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_VEHICLE_MODEL)));
                            details.setLatitude(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_LAT)));
                            details.setLongitude(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_LON)));
                            details.setLocationDetails(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_LOCATION_DETAILS)));
                            details.setDateTime(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_DATE_TIME)));
                            details.setSpeed(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_SPEED)));
                            details.setJourney(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_JOURNEY)));


                            lstDetails.add(details);

                        } while (fetchCur.moveToNext());
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Trying to close readable database
        try {

            fetchCur.close();
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lstDetails;
    }

    // fetch data from table
    public synchronized ArrayList<VehicleDetails> fetchDistinctCarList() {

        ArrayList<VehicleDetails> lstDetails = new ArrayList<VehicleDetails>();

        SQLiteDatabase db = null;
        Cursor fetchCur = null;
        // Trying to open readable database
        try {

            db = dbHelper.getReadableDatabase();
            if (db != null) {
                fetchCur = db.query(true, VEHICLE_STATUS_TABLE, null,
                        null
                        , null
                        , KEY_VEHICLE_MODEL, null, KEY_ROW_ID + " DESC", null);

                if (fetchCur != null) {
                    if (fetchCur.moveToFirst()) {
                        do {


                            VehicleDetails details = new VehicleDetails();

                            details.setRowId(fetchCur.getInt(fetchCur
                                    .getColumnIndexOrThrow(KEY_ROW_ID)));


                            details.setDriverName(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_DRIVER_NAME)));
                            details.setMobileNo(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_MOBILE_NO)));
                            details.setAddress(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_ADDRESS)));
                            details.setVehicleBrand(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_VECHICLE_BRAND)));
                            details.setVehicleModel(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_VEHICLE_MODEL)));
                            details.setLatitude(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_LAT)));
                            details.setLongitude(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_LON)));
                            details.setLocationDetails(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_LOCATION_DETAILS)));
                            details.setDateTime(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_DATE_TIME)));
                            details.setSpeed(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_SPEED)));
                            details.setJourney(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_JOURNEY)));


                            lstDetails.add(details);

                        } while (fetchCur.moveToNext());
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Trying to close readable database
        try {

            fetchCur.close();
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lstDetails;
    }


    // fetch data from table
    public synchronized ArrayList<String> fetchDriversList() {

        ArrayList<String> lstDetails = new ArrayList<String>();

        SQLiteDatabase db = null;
        Cursor fetchCur = null;
        // Trying to open readable database
        try {

            db = dbHelper.getReadableDatabase();
            if (db != null) {
                fetchCur = db.query(true, VEHICLE_STATUS_TABLE, null,
                        null
                        , null
                        , KEY_DRIVER_NAME, null, KEY_ROW_ID + " DESC", null);

                if (fetchCur != null) {
                    if (fetchCur.moveToFirst()) {
                        do {

                            lstDetails.add(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_DRIVER_NAME)));

                        } while (fetchCur.moveToNext());
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Trying to close readable database
        try {

            fetchCur.close();
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lstDetails;
    }


    // fetch data from table
    public synchronized ArrayList<String> fetchCarList() {

        ArrayList<String> lstDetails = new ArrayList<String>();

        SQLiteDatabase db = null;
        Cursor fetchCur = null;
        // Trying to open readable database
        try {

            db = dbHelper.getReadableDatabase();
            if (db != null) {
                fetchCur = db.query(true, VEHICLE_STATUS_TABLE, null,
                        null
                        , null
                        , KEY_VEHICLE_MODEL, null, KEY_ROW_ID + " DESC", null);

                if (fetchCur != null) {
                    if (fetchCur.moveToFirst()) {
                        do {

                            lstDetails.add(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_VEHICLE_MODEL)));

                        } while (fetchCur.moveToNext());
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Trying to close readable database
        try {

            fetchCur.close();
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lstDetails;
    }


    // fetch data from table
    public synchronized ArrayList<String> fetchjourneyList() {

        ArrayList<String> lstDetails = new ArrayList<String>();

        SQLiteDatabase db = null;
        Cursor fetchCur = null;
        // Trying to open readable database
        try {

            db = dbHelper.getReadableDatabase();
            if (db != null) {
                fetchCur = db.query(true, VEHICLE_STATUS_TABLE, null,
                        null
                        , null
                        , KEY_JOURNEY, null, KEY_ROW_ID + " DESC", null);

                if (fetchCur != null) {
                    if (fetchCur.moveToFirst()) {
                        do {

                            lstDetails.add(fetchCur.getString(fetchCur
                                    .getColumnIndexOrThrow(KEY_JOURNEY)));

                        } while (fetchCur.moveToNext());
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Trying to close readable database
        try {

            fetchCur.close();
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lstDetails;
    }


    // ==========================================================================================

    // method to delete all data from table
    public synchronized boolean deleteAllFromTable(String tableName) {
        boolean isDeleted = false;
        SQLiteDatabase db = null;
        // Trying to open writable database
        try {
            db = dbHelper.getWritableDatabase();
            if (db != null) {
                db.beginTransaction();
                isDeleted = db.delete(tableName, null, null) > 0;
                db.setTransactionSuccessful();
                db.endTransaction();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Trying to close writable database
        try {
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isDeleted;
    }

    // method to truncate all data from table
    public synchronized boolean truncateTable(String tableName) {
        boolean isDeleted = false;
        SQLiteDatabase db = null;
        // Trying to open writable database
        try {
            db = dbHelper.getWritableDatabase();
            if (db != null) {
                db.beginTransaction();

                if (tableName.trim().equals(VEHICLE_STATUS_TABLE.trim())) {
                    db.execSQL("DROP TABLE IF EXISTS " + VEHICLE_STATUS_TABLE);
                    db.execSQL(DbTable.createVehicleTable);
                    isDeleted = true;
                }

                db.setTransactionSuccessful();
                db.endTransaction();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Trying to close writable database
        try {
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isDeleted;


    }


    /////////////////////////////////////////////////////////////////
}