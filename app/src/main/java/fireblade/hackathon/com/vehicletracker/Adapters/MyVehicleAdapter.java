package fireblade.hackathon.com.vehicletracker.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import fireblade.hackathon.com.vehicletracker.Activity.MapsActivity;
import fireblade.hackathon.com.vehicletracker.Models.VehicleDetails;
import fireblade.hackathon.com.vehicletracker.R;
import fireblade.hackathon.com.vehicletracker.Util.AppConfig;
import fireblade.hackathon.com.vehicletracker.Util.Utility;

public class MyVehicleAdapter extends RecyclerView.Adapter<MyVehicleAdapter.MyViewHolder> {

    private List<VehicleDetails> lstDetails;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_srNo, tvVehicleName, tvDriverAssigned, tv_serviceDue;

        public MyViewHolder(View view) {
            super(view);
            tv_srNo = (TextView) view.findViewById(R.id.tv_srNo);
            tvVehicleName = (TextView) view.findViewById(R.id.tvVehicleName);
            tvDriverAssigned = (TextView) view.findViewById(R.id.tvDriverAssigned);
            tv_serviceDue = (TextView) view.findViewById(R.id.tv_serviceDue);

        }
    }


    public MyVehicleAdapter(List<VehicleDetails> list, Context context) {
        this.lstDetails = list;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_vehicle_list, parent, false);
        Utility.setFont(context, itemView,
                AppConfig.FONT_APP);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final VehicleDetails details = lstDetails.get(position);
        holder.tv_srNo.setText(position + 1 + ". ");
        holder.tvVehicleName.setText(details.getVehicleBrand()+" "+details.getVehicleModel());
        holder.tvDriverAssigned.setText(details.getDriverName());

        holder.tv_serviceDue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup(context,"Vehicle Service Due","\nVehicle Last Services: 06-JAN-2015\n Service Due Date: 05-JAN-2016");

            }
        });


    }


    @Override
    public int getItemCount() {
        return lstDetails.size();
    }


    public static void showPopup(Context context, String title, final String msg) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                context);
        if (!title.isEmpty()) {
            alertDialog.setTitle(title);
        }


        alertDialog.setMessage(msg);

        alertDialog.setPositiveButton("DISMISS",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();

                    }
                });

        alertDialog.show();
    }
}