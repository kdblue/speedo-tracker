package fireblade.hackathon.com.vehicletracker.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import fireblade.hackathon.com.vehicletracker.Activity.LoginActivity;
import fireblade.hackathon.com.vehicletracker.Activity.MainMenuNavActivity;
import fireblade.hackathon.com.vehicletracker.Adapters.DriverProfileAdapter;
import fireblade.hackathon.com.vehicletracker.Adapters.MyVehicleAdapter;
import fireblade.hackathon.com.vehicletracker.Models.VehicleDetails;
import fireblade.hackathon.com.vehicletracker.R;
import fr.ganfra.materialspinner.MaterialSpinner;

public class MyVehicleFragment extends Fragment {

    Context context;
    MaterialSpinner spnDriver, spnCar, spnjourney;
    private static RecyclerView recyclerViewMyVehicle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.my_vehicle_frag, container, false);
        MainMenuNavActivity.toolbar.setTitle(getActivity().getResources().getString(R.string.my_vehicle));
        recyclerViewMyVehicle = (RecyclerView) view.findViewById(R.id.recyclerViewMyVehicle);

        context=getActivity();
        bindRecyclerView(context);
        return view;
    }

    private void bindRecyclerView(Context context) {
        ///////////////////////////////
        ArrayList<VehicleDetails> lstDetails = LoginActivity.dbAdapter.fetchDistinctCarList();


        //////////////////////////////////////

        if (lstDetails.isEmpty()) {
            recyclerViewMyVehicle.setVisibility(View.GONE);
        } else {
            recyclerViewMyVehicle.setVisibility(View.VISIBLE);
            MyVehicleAdapter mAdapter = new MyVehicleAdapter(lstDetails, context);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
            recyclerViewMyVehicle.setLayoutManager(mLayoutManager);
            recyclerViewMyVehicle.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        }


    }

}
   