package fireblade.hackathon.com.vehicletracker.NavDrawer;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.style.TypefaceSpan;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import fireblade.hackathon.com.vehicletracker.Activity.MainMenuNavActivity;
import fireblade.hackathon.com.vehicletracker.Fragments.DriversProfileFragment;
import fireblade.hackathon.com.vehicletracker.Fragments.MyVehicleFragment;
import fireblade.hackathon.com.vehicletracker.Fragments.VehicleStatusFragment;
import fireblade.hackathon.com.vehicletracker.R;
import fireblade.hackathon.com.vehicletracker.Util.AppConfig;
import fireblade.hackathon.com.vehicletracker.Util.CustomTypefaceSpan;
import fireblade.hackathon.com.vehicletracker.Util.Utility;


public class NavigationDrawerMenuFragment extends com.mxn.soul.flowingdrawer_core.MenuFragment {
    public static NavigationView vNavigation;
    static Context context;
    static AppCompatActivity activity;
    private AlertDialog alertDialogForAppInfo;

    TypefaceSpan robotoRegularSpan;

    private View referenceView;
    AlertDialog.Builder builder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu_nav, container,
                false);
        context = getActivity();
        activity = (AppCompatActivity) getActivity();
        RelativeLayout rl_main = (RelativeLayout) view.findViewById(R.id.rl_main);
        // Setting fonts to all views
        Utility.font = Typeface.createFromAsset(getActivity().getAssets(), AppConfig.FONT_APP);
        Utility.setFont(getActivity(), rl_main,
                AppConfig.FONT_APP);
        robotoRegularSpan = new CustomTypefaceSpan("", Utility.font);

        vNavigation = (NavigationView) view.findViewById(R.id.vNavigation);
        FrameLayout fm_main = (FrameLayout) vNavigation.getHeaderView(0);

        fm_main.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
//                Toast.makeText(context, "Long Pressed", Toast.LENGTH_LONG).show();
                return false;
            }
        });

        setupNavigation();
        return setupReveal(view);
    }

    public void onOpenMenu() {

    }

    public void onCloseMenu() {
//        Toast.makeText(getActivity(), "onCloseMenu", Toast.LENGTH_SHORT).show();
    }

    private static void setupNavigation() {
        try {
            vNavigation.getMenu().findItem(R.id.vehicle_status).setChecked(true);
            //we are getting action bar bug as it is disable we need to re assign if


            NavigationDrawerMenuFragment.vNavigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(final MenuItem item) {


                    try {
                        //if layout is greater than lollypo then make actionbar scrollable
                        if (Build.VERSION.SDK_INT >= 21) {
                            AppBarLayout.LayoutParams params =
                                    (AppBarLayout.LayoutParams) MainMenuNavActivity.toolbar.getLayoutParams();
                            params.setScrollFlags(0);


                        } else {
                            //do nothing
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    switch (item.getItemId()) {
                        case R.id.vehicle_status:

                            MainMenuNavActivity.fragmentClass = VehicleStatusFragment.class;
                            break;

                        case R.id.drivers_details:
                            MainMenuNavActivity.fragmentClass = DriversProfileFragment.class;
                            break;

                        case R.id.my_vehicles:
                            MainMenuNavActivity.fragmentClass = MyVehicleFragment.class;
                            break;

                        case R.id.menu_exit:
                            exitConfirmation();
                            break;


                        default:
                            return false;
                    }


                    try {
                        //to speed up the operation
                        Handler mHandler;
                        mHandler = new Handler();
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (item.getItemId() != R.id.menu_exit) {


                                    try {
                                        MainMenuNavActivity.fragment = (Fragment) MainMenuNavActivity.fragmentClass.newInstance();
                                    } catch (Fragment.InstantiationException e) {
                                        e.printStackTrace();
                                    } catch (IllegalAccessException e) {
                                        e.printStackTrace();
                                    } catch (java.lang.InstantiationException e) {
                                        e.printStackTrace();
                                    }

                                    //animations
                                    FragmentTransaction transaction = MainMenuNavActivity.fragmentManager.beginTransaction();
                                    transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);

                                    transaction.replace(R.id.flContent, MainMenuNavActivity.fragment);
                                    transaction.addToBackStack(null);
                                    transaction.commit();
                                }

                            }
                        }, 300);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //navigate and close the drawer
                    item.setChecked(true);
                    MainMenuNavActivity.mLeftDrawerLayout.closeDrawer();

                    return false;
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private static void exitConfirmation() {
        final DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        try {

                            activity.finish();


                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };
        String msg = "Are you sure you want to exit?";

        String yesmsg = "Yes";


        String nomsg = "No";
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage((msg))
                .setPositiveButton((yesmsg), dialogClickListener)
                .setNegativeButton((nomsg), dialogClickListener).show();
    }

}
