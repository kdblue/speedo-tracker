/**
 *
 */
package fireblade.hackathon.com.vehicletracker.Util;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

import fireblade.hackathon.com.vehicletracker.R;


/**
 * @author siddhesh@viraat.info
 * @since 2016
 */
public class Utility {

    public static Typeface font = null;
    public static Typeface robotoRegular = null;
    public static AlertDialog alertMsg = null;
    public static ProgressDialog pd = null;


    /**
     * Method to set font to all views
     *
     * @param context
     * @param root     parent view
     * @param fontName font name
     */
    public static void setFont(final Context context, final View root,
                               String fontName) {
        try {
//            font = Typeface.createFromAsset(context.getAssets(), fontName);

            if (root instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) root;
                for (int i = 0; i < viewGroup.getChildCount(); i++)
                    setFont(context, viewGroup.getChildAt(i), fontName);
            } else if (root instanceof EditText) {
                ((EditText) root).setTypeface(font);
            } else if (root instanceof AutoCompleteTextView) {
                ((AutoCompleteTextView) root).setTypeface(font);
            } else if (root instanceof TextView) {
                ((TextView) root).setTypeface(font);
            } else if (root instanceof Button) {
                ((Button) root).setTypeface(font);
            } else if (root instanceof CheckBox) {
                ((CheckBox) root).setTypeface(font);
            } else if (root instanceof RadioButton) {
                ((RadioButton) root).setTypeface(font);
            } else if (root instanceof TabWidget) {
                ((TextView) root).setTypeface(font);
            } else if (root instanceof TabHost) {
                ((TextView) root).setTypeface(font);
            }
        } catch (Exception e) {
            Log.e("Assets tagging", String.format(
                    "Error occured when trying setting font %s view", root));
            e.printStackTrace();
        }
    }

    public static void removeErrorOnTextChange(final EditText et) {
        et.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                et.setError(null);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }


    public static boolean isInternetConnectionAvailable(Context context) {
        ConnectivityManager connec = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connec != null
                && (connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED)
                || (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED)) {
            return true;
        } else {
            return false;
        }
    }


    public static void showPopup(Context context, String title, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(title);

        builder.setMessage(msg).setCancelable(false)
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertMsg = builder.create();
        alertMsg.show();
    }




}