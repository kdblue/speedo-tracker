package fireblade.hackathon.com.vehicletracker.Fragments;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.util.ArrayList;

import fireblade.hackathon.com.vehicletracker.Activity.LoginActivity;
import fireblade.hackathon.com.vehicletracker.Activity.MainMenuNavActivity;
import fireblade.hackathon.com.vehicletracker.Adapters.DriverProfileAdapter;
import fireblade.hackathon.com.vehicletracker.Adapters.VehicleStatusAdapter;
import fireblade.hackathon.com.vehicletracker.Models.VehicleDetails;
import fireblade.hackathon.com.vehicletracker.R;
import fr.ganfra.materialspinner.MaterialSpinner;

public class DriversProfileFragment extends Fragment {

    Context context;
    MaterialSpinner spnDriver, spnCar, spnjourney;
    private static RecyclerView recyclerDriverProfile;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.driver_profile_frag, container, false);
        MainMenuNavActivity.toolbar.setTitle(getActivity().getResources().getString(R.string.drivers_profile));
        recyclerDriverProfile = (RecyclerView) view.findViewById(R.id.recyclerDriverProfile);

        context=getActivity();
        bindRecyclerView(context);
        return view;
    }

    private void bindRecyclerView(Context context) {
        ///////////////////////////////
        ArrayList<VehicleDetails> lstDetails = LoginActivity.dbAdapter.fetchDistinctDriverList();


        //////////////////////////////////////

        if (lstDetails.isEmpty()) {
            recyclerDriverProfile.setVisibility(View.GONE);
        } else {
            recyclerDriverProfile.setVisibility(View.VISIBLE);
            DriverProfileAdapter mAdapter = new DriverProfileAdapter(lstDetails, context);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
            recyclerDriverProfile.setLayoutManager(mLayoutManager);
            recyclerDriverProfile.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        }


    }

}
   