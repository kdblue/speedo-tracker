/**
 *
 */
package fireblade.hackathon.com.vehicletracker.Adapters;


import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;

import fireblade.hackathon.com.vehicletracker.R;
import fireblade.hackathon.com.vehicletracker.Util.AppConfig;
import fireblade.hackathon.com.vehicletracker.Util.Utility;

/**
 * @author siddhesh@viraat.info since 2016
 */
public class SpinnerAdapter extends ArrayAdapter<String> {

    private Context context;
    private int row_layout;
    private ArrayList<String> objects;

    Typeface font;
    /**
     * @param context
     * @param resource
     * @param data
     */
    public SpinnerAdapter(Context context, int resource,
                           ArrayList<String> data) {
        super(context, resource, data);
        this.context = context;
        this.row_layout = resource;
        this.objects = data;

        font = Typeface.createFromAsset(context.getAssets(),
                AppConfig.FONT_APP);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(row_layout, null);
            Utility.setFont(context,convertView,AppConfig.FONT_APP);
        }

        TextView tv_text = (TextView) convertView.findViewById(R.id.tv_text);
        tv_text.setText(objects.get(position).toString().trim());
        tv_text.setTextSize(17);
        tv_text.setTypeface(font);

        return convertView;
    }

    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View v = super.getDropDownView(position, convertView, parent);

        ((TextView) v).setTypeface(font);
        ((TextView) v).setText(objects.get(position).toString().trim());

        return v;
    }
}
