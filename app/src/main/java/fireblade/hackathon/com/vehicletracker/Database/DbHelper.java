package fireblade.hackathon.com.vehicletracker.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class
/**
 * @author siddhesh@viraat.info
 * @since 2016
 */
DbHelper extends SQLiteOpenHelper {
	private static final String DatabaseName = "OmniReach.db";
	private static final int DatabaseVersion = 1;

	public DbHelper(Context context) {
		super(context, DatabaseName, null, DatabaseVersion);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		DbTable.onCreate(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int Old_Ver, int New_Ver) {
		DbTable.onUpgrade(db, Old_Ver, New_Ver);
	}

}