package fireblade.hackathon.com.vehicletracker.Activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mxn.soul.flowingdrawer_core.FlowingView;
import com.mxn.soul.flowingdrawer_core.LeftDrawerLayout;

import java.util.ArrayList;
import java.util.logging.Handler;

import fireblade.hackathon.com.vehicletracker.Fragments.VehicleStatusFragment;
import fireblade.hackathon.com.vehicletracker.Models.VehicleDetails;
import fireblade.hackathon.com.vehicletracker.NavDrawer.NavigationDrawerMenuFragment;
import fireblade.hackathon.com.vehicletracker.R;
import fireblade.hackathon.com.vehicletracker.Util.AppConfig;
import fireblade.hackathon.com.vehicletracker.Util.Utility;
import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt;


public class MainMenuNavActivity extends AppCompatActivity {
    //flowing view
    public static FlowingView mFlowingView;
    public static LeftDrawerLayout mLeftDrawerLayout;
    /////////////////////////init fragments
    public static Fragment fragment = null;
    public static Class fragmentClass = null;
    public static FragmentManager fragmentManager;
    CoordinatorLayout coordinatorLayout;
    public static Activity activity;
    static Context context;
    public static RelativeLayout rl_main;
    //    public static TabLayout tabLayout;
    public static Toolbar toolbar;
    public static AppBarLayout appBarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /////////////////////////////////////////////////////////////////////
        activity = MainMenuNavActivity.this;
        context = MainMenuNavActivity.this;
        // Setting fonts to all views
        rl_main = (RelativeLayout) findViewById(R.id.rl_main);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.content);
        Utility.font = Typeface.createFromAsset(getAssets(), AppConfig.FONT_APP);
        Utility.setFont(context, rl_main,
                AppConfig.FONT_APP);

        appBarLayout = (AppBarLayout) findViewById(R.id.appBarLayout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Helloo");
        toolbar.setNavigationIcon(R.mipmap.ic_menu_white_24dp);
        //set tool bar font
        for (int i = 0; i < toolbar.getChildCount(); i++) {
            View view = toolbar.getChildAt(i);
            if (view instanceof TextView) {
                TextView tv = (TextView) view;
                tv.setTypeface(Utility.font);
                break;
            }
        }

        setSupportActionBar(toolbar);


        mLeftDrawerLayout = (LeftDrawerLayout) findViewById(R.id.id_drawerlayout);

        fragmentManager = getSupportFragmentManager();
        FragmentManager fm = getSupportFragmentManager();
        NavigationDrawerMenuFragment mMenuFragment = (NavigationDrawerMenuFragment) fm.findFragmentById(R.id.frame_container_menu);
        mFlowingView = (FlowingView) findViewById(R.id.navigationFlowView);


        if (mMenuFragment == null) {
            fm.beginTransaction().add(R.id.frame_container_menu, mMenuFragment = new NavigationDrawerMenuFragment()).commit();
        }
        mLeftDrawerLayout.setFluidView(mFlowingView);
        mLeftDrawerLayout.setMenuFragment(mMenuFragment);

        showTabPreview();
        openHomeFragment(savedInstanceState);
    }

    private void openHomeFragment(Bundle savedInstanceState) {
        if (savedInstanceState == null) {

            try {
                fragmentClass = VehicleStatusFragment.class;
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            mLeftDrawerLayout.openDrawer();
        }


        return super.onOptionsItemSelected(menuItem);
    }

    private void showTabPreview() {

        android.os.Handler mHandler;
        mHandler = new android.os.Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                final MaterialTapTargetPrompt.Builder tapTargetPromptBuilder = new MaterialTapTargetPrompt.Builder(MainMenuNavActivity.this)
                        .setPrimaryText("Navigation Menu")
                        .setSecondaryText("You can view the menus here..")
                        .setAnimationInterpolator(new FastOutSlowInInterpolator())
                        .setMaxTextWidth(R.dimen.tap_target_menu_max_width).setCaptureTouchEventOnFocal(true)
                        .setCaptureTouchEventOutsidePrompt(false)
                        .setBackgroundColour(getResources().getColor(R.color.black))
                        .setFocalColour(getResources().getColor(R.color.focul_color1))
                        .setCaptureTouchEventOutsidePrompt(false)
                        .setTarget(toolbar.getChildAt(1));

                tapTargetPromptBuilder.setOnHidePromptListener(new MaterialTapTargetPrompt.OnHidePromptListener() {
                    @Override
                    public void onHidePrompt(MotionEvent event, boolean tappedTarget) {
                        //Do something such as storing a value so that this prompt is never shown again
                        android.os.Handler mHandler;
                        mHandler = new android.os.Handler();
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                final MaterialTapTargetPrompt.Builder tapTargetPromptBuilder = new MaterialTapTargetPrompt.Builder(MainMenuNavActivity.this)
                                        .setPrimaryText("Refresh")
                                        .setSecondaryText("You can refresh your content here..")
                                        .setAnimationInterpolator(new FastOutSlowInInterpolator())
                                        .setMaxTextWidth(R.dimen.tap_target_menu_max_width)
                                        .setBackgroundColour(getResources().getColor(R.color.tab_preview_background1))
                                        .setFocalColour(getResources().getColor(R.color.focul_color1))
                                        .setTarget(R.id.menu_refresh);


                                tapTargetPromptBuilder.show();


                            }
                        }, 500);
                    }

                    @Override
                    public void onHidePromptComplete() {

                    }
                });
                tapTargetPromptBuilder.show();
            }
        }, 1000);
    }
}
