package fireblade.hackathon.com.vehicletracker.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import fireblade.hackathon.com.vehicletracker.Activity.LoginActivity;
import fireblade.hackathon.com.vehicletracker.Activity.MainMenuNavActivity;
import fireblade.hackathon.com.vehicletracker.Adapters.SpinnerAdapter;
import fireblade.hackathon.com.vehicletracker.Adapters.VehicleStatusAdapter;
import fireblade.hackathon.com.vehicletracker.Models.VehicleDetails;
import fireblade.hackathon.com.vehicletracker.R;
import fr.ganfra.materialspinner.MaterialSpinner;

public class VehicleStatusFragment extends Fragment {

    Context context;
    MaterialSpinner spnDriver, spnCar, spnjourney;
    private static RecyclerView recyclerViewVehicleStatus;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.vehicle_status_frag, container, false);
        MainMenuNavActivity.toolbar.setTitle(getActivity().getResources().getString(R.string.vehicle_status));
        context = getActivity();


        spnDriver = (MaterialSpinner) view.findViewById(R.id.spnDriver);
        spnCar = (MaterialSpinner) view.findViewById(R.id.spnCar);
        spnjourney = (MaterialSpinner) view.findViewById(R.id.spnjourney);
        recyclerViewVehicleStatus = (RecyclerView) view.findViewById(R.id.recyclerViewVehicleStatus);

        bindSpinners();
        bindRecyclerView(context);
        setHasOptionsMenu(true);
        return view;
    }

    private void bindSpinners() {
        //get the data from db
        ArrayList<String> lstDrivers = LoginActivity.dbAdapter.fetchDriversList();
        ArrayList<String> lstCars = LoginActivity.dbAdapter.fetchCarList();
        ArrayList<String> lstJourneys = LoginActivity.dbAdapter.fetchjourneyList();


        SpinnerAdapter driverAdapter = new SpinnerAdapter(context, R.layout.custom_spn_adapter, lstDrivers);
        driverAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnDriver.setAdapter(driverAdapter);

        SpinnerAdapter carAdapter = new SpinnerAdapter(context, R.layout.custom_spn_adapter, lstCars);
        carAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnCar.setAdapter(carAdapter);

        SpinnerAdapter journeyAdapter = new SpinnerAdapter(context, R.layout.custom_spn_adapter, lstJourneys);
        journeyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnjourney.setAdapter(journeyAdapter);
    }

    private void bindRecyclerView(Context context) {
        ///////////////////////////////
        ArrayList<VehicleDetails> lstDetails = LoginActivity.dbAdapter.fetchVehicleStatsuList();


        //////////////////////////////////////

        if (lstDetails.isEmpty()) {
            recyclerViewVehicleStatus.setVisibility(View.GONE);
        } else {
            recyclerViewVehicleStatus.setVisibility(View.VISIBLE);
            VehicleStatusAdapter mAdapter = new VehicleStatusAdapter(lstDetails, context);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
            recyclerViewVehicleStatus.setLayoutManager(mLayoutManager);
            recyclerViewVehicleStatus.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        }


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        menu.clear();
        inflater.inflate(R.menu.main_menu_refresh, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

}
   