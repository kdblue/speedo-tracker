package fireblade.hackathon.com.vehicletracker.Models;


public class LoginDetails {
    String userName;
    String password;
    public LoginDetails() {

            this.userName = "";
        this.password = "";
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
