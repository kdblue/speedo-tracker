package fireblade.hackathon.com.vehicletracker.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import fireblade.hackathon.com.vehicletracker.Activity.LoginActivity;
import fireblade.hackathon.com.vehicletracker.Activity.MapsActivity;
import fireblade.hackathon.com.vehicletracker.Models.VehicleDetails;
import fireblade.hackathon.com.vehicletracker.R;
import fireblade.hackathon.com.vehicletracker.Util.AppConfig;
import fireblade.hackathon.com.vehicletracker.Util.Utility;

public class VehicleStatusAdapter extends RecyclerView.Adapter<VehicleStatusAdapter.MyViewHolder> {

    private List<VehicleDetails> lstDetails;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_srNo, tv_time, tv_speed, tv_trck;

        public MyViewHolder(View view) {
            super(view);
            tv_srNo = (TextView) view.findViewById(R.id.tv_srNo);
            tv_time = (TextView) view.findViewById(R.id.tv_time);
            tv_speed = (TextView) view.findViewById(R.id.tv_speed);
            tv_trck = (TextView) view.findViewById(R.id.tv_trck);

        }
    }


    public VehicleStatusAdapter(List<VehicleDetails> list, Context context) {
        this.lstDetails = list;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.vehicle_status_list, parent, false);
        Utility.setFont(context, itemView,
                AppConfig.FONT_APP);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final VehicleDetails details = lstDetails.get(position);

        holder.tv_srNo.setText(position + 1 + ". ");
        holder.tv_time.setText(details.getDateTime());
        holder.tv_speed.setText(details.getSpeed() + " km/hr");

        holder.tv_trck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                MapsActivity.vehicleDetails = details;
                Intent in = new Intent(context, MapsActivity.class);
                context.startActivity(in);
            }
        });


    }


    @Override
    public int getItemCount() {
        return lstDetails.size();
    }
}